<%-- 
    Document   : response
    Created on : Jun 24, 2019, 12:21:27 AM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:useBean id="mybean" scope="session" class="org.mypackage.bulbapp.WifibulbApp" />
        <jsp:setProperty name="mybean" property="ipaddress"  />
        <h1>Ip address</h1>
        <p>ip address: <jsp:getProperty name="mybean" property="ipaddress" /> </p>
    </body>
</html>
