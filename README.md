#iot
IoT PROJECT
GROUP MEMBERS
1.Kevin K Ngobiro
2.Willie N Macharia
3.Vernon Basweti
4.John Siele
5.Brian Kipkorir
6.Steve Augo
7.vincent were

INTERNET CONTROLLED BULB

EQUIPMENT NEEDED(HARDWARE)
1.node MCU board (WiFi Development Board).
2.Relay Switch
3.LED Bulb
4.Laptop
 SOFTWARE REQUIREMENTS
1.Arduino
2.android studio IDE

TASK DISTRIBUTION
a)WireFrames
    Vernon Basweti 
    Brian Kipkorir.
b)ADRUINO   
    kevin ngobiro
c)Droid
    Willie Macharia
d)Hardware Collection
    Kevin Ngobiro 
    willie Macharia
e)UI design 
    John Siele
    Steve Augo 
    vincent were
f)Coding
     Everyone
More specs to come

g)Hardware Connection and implementation
     Everyone

h)Items to add
 -cloud computing
 -AI(smart room)
 