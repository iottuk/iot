package com.example.chatbot2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    Button cancelbtn;
    Button exitbtn;
    Button sendbtn;
    TextView view;
    Text edittxt;

    //USER SPEECH
    String MSG;
    public String getText(){
       String MSG=edittxt.getTextContent();
       edittxt.setData("");
       view.append("ME:" + MSG + "\n");
        return MSG;
    }

    //BOT SPEECH
    public void bot(String s){
        view.append("BOT:"+s+"\n");
    }

    public void viewtext(){
        getText();
        if (MSG.contains("hi")) {
            int rnd=(int)(Math.random()*3 +1);
            if (rnd==1) {
                bot("Hi!");
            }
            else if(rnd==2) {
                bot("Hello!");
            }
            else if(rnd==3){
                bot("Holla!");
            }
            else{
                bot("How are you");
            }
        }else if(MSG.contains("morning")||MSG.contains("mng")||MSG.contains("good morning")){
            bot("Morning too!");
        }
        else if(MSG.isEmpty()){
            bot("WRITE SOMETHING");
            /*
             */
        }else
        {

            int rand=(int) (Math.random()* 3 +1);
            if(rand==1)
            {
                bot("Am just a bot,I can't understand that");
            }
            else if(rand==2)
            {
                bot("cant understand that please");
            }
            else if(rand==1)
            {
                bot("useless");
            }
            else
            {
                bot("wewe,gunia!");
            }
        }
    }


        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

////////////////////////////////////////////////////////////////////////////
        sendbtn= findViewById(R.id.sendbtn);
        cancelbtn=findViewById(R.id.cancelbtn);
        edittxt=findViewById(R.id.edittxt);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent act = new Intent();
                act.setAction(Intent.ACTION_VIEW);
                //act.setData(Uri.parse(startNextMatchingActivity()));
                startActivity(act);
            }
        });

        sendbtn.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  viewtext();
                  getText();
              }
        });

        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Intent   inte =new Intent(getApplicationContext(),next.class);
            startActivity(inte);


            }
        });
    }
}




